

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import nDollarRecognizer.*;


public class GlassPane extends JComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TestWindow container;

	private ArrayList<Tuple[]> strokes;
	private ArrayList<Tuple> currStroke;
	private boolean paintStrokes = true;
	
	NDollarRecognizer recognizer;
	
	private String[] names;

	protected void paintComponent(Graphics g) {
		if (paintStrokes) {
			if (!(strokes.size() == 0)) {
				g.setColor(Color.red);
				for (int i = 0; i < strokes.size(); i++)
				{
					for (int j = 1; j < strokes.get(i).length; j++)
					{
						g.drawLine((int)strokes.get(i)[j-1].x, (int)strokes.get(i)[j-1].y,
								(int)strokes.get(i)[j].x, (int)strokes.get(i)[j].y);
					}
				}
			}
		}
	}
	
	public GlassPane(TestWindow contentPane, int width, int height)
	{
		container = contentPane;
		strokes = new ArrayList<Tuple[]>();
		
		ArrayList<Multistroke> baseSymbols = new ArrayList<Multistroke>();
		
		//Tuple[][] t = {{new Tuple(30, 7), new Tuple(103, 7)}, {new Tuple(66, 7), new Tuple(66, 87)}};
		//Tuple[][] n = {{new Tuple(177, 92), new Tuple(177, 2)}, {new Tuple(182, 1), new Tuple(246, 95)}, {new Tuple(247, 87), new Tuple(247, 1)}};
		
		//Multistroke mT = new Multistroke(0, true, t);
		//Multistroke mN = new Multistroke(1, true, n);
		
		//baseSymbols.add(mT);
		//baseSymbols.add(mN);
		
		
		
		TemplateManager tm = new TemplateManager();
        ArrayList<Symbol> templates = tm.getTemplates();
        names = new String[71];
        int index = 0;
        for (Symbol s : templates)
        {
            baseSymbols.add(new Multistroke(index, true, s.getStrokes()));
            names[index] = s.getName();
            index++;
        }
        int[] indices = new int[index];
        for (int i = 0; i < index; i++)
        {
            indices[i] = i;
        }
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa " + index);
        
		
		recognizer = new NDollarRecognizer(Math.max(width, height), true);
		
		recognizer.setMultistrokes(baseSymbols);
		recognizer.setUseIndices(indices);
		
		GlassPaneListener listener = new GlassPaneListener(this);
		this.addMouseListener(listener);
		this.addMouseMotionListener(listener);
	}
	
	private class GlassPaneListener extends MouseInputAdapter
	{
		
		public GlassPaneListener(GlassPane pane) {}
		
		public void mouseDragged(MouseEvent e)
		{
			if (SwingUtilities.isLeftMouseButton(e))
			{
				currStroke.add(new Tuple(e.getPoint().x, e.getPoint().y));
				repaint();
			}
		}
		
		public void mousePressed(MouseEvent e)
		{
			if (SwingUtilities.isLeftMouseButton(e))
			{
				currStroke = new ArrayList<Tuple>();
			}
			else if (SwingUtilities.isRightMouseButton(e))
			{
//				Tuple[][] eh = (Tuple[][])strokes.toArray(new Tuple[0][]);
//				for (int i = 0; i < eh.length; i++)
//				{
//					for (int j = 0; j < eh[i].length; j++)
//						System.out.println(eh[i][j].x + " - " + eh[i][j].y);
//				}
				NDollarRecognizer.Result result = recognizer.recognize((Tuple[][])strokes.toArray(new Tuple[0][]), true, false);
				if (result != null)
				{
					container.setStatus(names[result.pose] + " score: " + Double.toString(result.score));
				}
				else
				{
					container.setStatus("Didn't work");
				}
				
				strokes.clear();
				repaint();
				
			}
		}
		
		public void mouseReleased(MouseEvent e)
		{
			if (e.getButton() == MouseEvent.BUTTON1)
			{
				strokes.add((Tuple[])currStroke.toArray(new Tuple[0]));
				repaint();
			}
		}
	}

}

