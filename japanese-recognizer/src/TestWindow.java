import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class TestWindow extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel status;
	GlassPane pane;
	
	public TestWindow()
	{
		super("Test Window");
		setupUI();
	}
	
	private void setupUI()
	{
		int width = 600;
		int height = 400;
		
		this.setPreferredSize(new Dimension(width, height));
		
		pane = new GlassPane(this, width, height);
		this.setGlassPane(pane);
		pane.setVisible(true);
		
		status = new JLabel("Status");
		this.add(status, BorderLayout.SOUTH);
		
		this.pack();
		this.setVisible(true);
	}
	
	public void setStatus(String newStatus)
	{
		status.setText(newStatus);
	}

}
