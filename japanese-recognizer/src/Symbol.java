import nDollarRecognizer.Tuple;

public class Symbol 
{
	private String name;
	private String language;
	private Tuple[][] strokes;
	
	public Symbol(String name, Tuple[][] strokes) 
	{
		this.name = name;
		this.strokes = strokes;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Tuple[][] getStrokes()
	{
		return this.strokes;
	}
	
	public String getLanguage()
	{
		return this.language;
	}

}
