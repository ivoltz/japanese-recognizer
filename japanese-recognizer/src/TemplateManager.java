import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import nDollarRecognizer.Tuple;

public class TemplateManager {
	
	private ArrayList<Symbol> templates;
	
	public TemplateManager()
	{
		templates = new ArrayList<Symbol>();
		loadFile();
	}
	
	public void loadFile() 
	{
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try 
		{
		    final DocumentBuilder builder = factory.newDocumentBuilder();
		    final Document document = builder.parse(new File("kanas.xml"));
		    
		    //Affiche la version de XML
			System.out.println(document.getXmlVersion());

			//Affiche l'encodage
			System.out.println(document.getXmlEncoding());	

			//Affiche s'il s'agit d'un document standalone		
			System.out.println(document.getXmlStandalone());
			
			final Element root = document.getDocumentElement();
			System.out.println(root.getNodeName());
			
			final NodeList rootNodes = root.getChildNodes();			
			final int nbRacineNoeuds = rootNodes.getLength();
			
			for (int i = 0; i < nbRacineNoeuds; i++) 
			{
		        if(rootNodes.item(i).getNodeType() == Node.ELEMENT_NODE) 
		        {
		            Element template = (Element) rootNodes.item(i);
		            String name = template.getAttribute("name");
		            NodeList strokes = template.getElementsByTagName("stroke");
		            int numberOfStrokes = strokes.getLength();
		            Tuple[][] data = new Tuple[numberOfStrokes][];
		            
		            for (int j = 0; j < numberOfStrokes; j++)
		            {
		            	Element stroke = (Element) strokes.item(j);
		            	NodeList points = stroke.getElementsByTagName("point");
		            	int numberOfPoints = points.getLength();
		            	ArrayList<Tuple> pointList = new ArrayList<Tuple>();
		            	
		            	for (int k = 0; k < numberOfPoints; k++)
		            	{
		            		final Element point = (Element) points.item(k);
		            		pointList.add(new Tuple(Integer.parseInt(point.getAttribute("x")), 
		            				Integer.parseInt(point.getAttribute("y"))));
		            	}
		            	data[j] = pointList.toArray(new Tuple[0]);
		            }
		            templates.add(new Symbol(name, data));		           
		        }
			}
		} catch (final ParserConfigurationException e) 
		{
		    e.printStackTrace();
		} catch (final SAXException e) 
		{
		    e.printStackTrace();
		} catch (final IOException e) 
		{
		    e.printStackTrace();
		}
		
	}
	
	public ArrayList<Symbol> getTemplates()
	{
		return templates;
	}

}
