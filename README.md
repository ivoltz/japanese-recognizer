# Kanas Teacher
## HCI909 project

An Android application teaching Japanese kanas  by __Béasse Félix__ and
__Voltz Iann__ as part of the HCI909 (Advanced Programming of Interactive
    Systems) class.

### Technical description
This **Android application** teaches Japanese kanas by showing how to draw them
and allows the user to test their knowledge by letting them draw a kana to get
a score from an implementation of the **$N Recognizer$**.

### Run the application
To use the app, either open the `KanasTeacher` project (the **KanasTeacher**
folder) with **Android Studio** and run the app with your Android phone
connected or put the **kanas-teacher.apk** file on your phone and open it to
install the app (you might need to allow the installation of apps from unknown
sources).
