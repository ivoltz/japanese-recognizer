package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beassevoltz.hci.kanasteacher.R;
import com.beassevoltz.hci.kanasteacher.Utils.Globals;

import java.io.InputStream;

public class SplashScreenActivity extends AppCompatActivity {

    int backButtonCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Use fullscreen for this activity
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // Setup loading bar
        ProgressBar spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.VISIBLE);

        // Setup loading text animation
        final TextView loadingtext = findViewById(R.id.loadingText);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        loadingtext.startAnimation(anim);


        new LoadingAsyncTask().execute();

    }

    /**
     * Get the InputStream from the symbols file
     * @return The symbols file's InputStream
     */
    private InputStream returnXmlInputStream()
    {
        try
        {
            return getResources().openRawResource(R.raw.kanas);
        }
        catch (final Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Closes the application if back is pressed twice
     */
    @Override
    public void onBackPressed()
    {
        if (backButtonCount > 0)
        {
            finish();
            System.exit(0);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Press the back button again to exit the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

    /**
     * Private ASyncTask used to load the symbols from an XML file
     */
    private class LoadingAsyncTask extends AsyncTask<Void, Void, Void>
    {

        /**
         * Loads the symbols in the Globals class
         * @param voids
         * @return
         */
        @Override
        protected Void doInBackground(Void... voids) {
            Globals globals = Globals.getInstance();
            globals.setupRecognizer(returnXmlInputStream());
            return null;
        }

        /**
         * Closes the activity afterwards
         * @param param
         */
        @Override
        protected void onPostExecute(Void param)
        {
            Intent intent = new Intent(SplashScreenActivity.this, TitleScreenActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }
    }
}
