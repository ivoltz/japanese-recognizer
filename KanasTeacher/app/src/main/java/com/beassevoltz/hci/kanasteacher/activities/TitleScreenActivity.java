package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.beassevoltz.hci.kanasteacher.R;

public class TitleScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_screen);

        // Use fullscreen for this activity
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // Setup the "Press to begin" text animation
        final TextView beginText = findViewById(R.id.beginText);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(2000);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        beginText.startAnimation(anim);

        // Setup the listener when pressing the screen
        final ConstraintLayout titleLayout = findViewById(R.id.titleLayout);
        titleLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent i = new Intent(TitleScreenActivity.this, MainMenuActivity.class);
                startActivity(i);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                TitleScreenActivity.this.finish();
                return false;
            }
        });
    }

    /**
     * Closes the app when back is pressed
     */
    @Override
    public void onBackPressed()
    {
        finish();
        System.exit(0);
    }


}
