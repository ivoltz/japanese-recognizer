package com.beassevoltz.hci.kanasteacher.database;

import android.util.Xml;

import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Tuple;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Parses the kanas.xml file to extract the kanas in a ArrayList of Symbol
 * All functions correspond to a level of depth in the xml file.
 */

public class XmlParser
{
    private static final String ns = null;

    public ArrayList<Symbol> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private ArrayList<Symbol> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Symbol> templates = new ArrayList<Symbol>();
        parser.require(XmlPullParser.START_TAG, ns, "kanaTemplates");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the template tag
            if (name.equals("template")) {
                String symbolName = parser.getAttributeValue(null, "name");
                String symbolRomaji = parser.getAttributeValue(null, "romaji");
                int numberOfStrokes = Integer.parseInt(parser.getAttributeValue(null, "numberOfStrokes"));
                templates.add(readTemplate(numberOfStrokes, symbolName, symbolRomaji, parser));
            } else {
                skip(parser);
            }
        }
        return templates;
    }


    private Symbol readTemplate(int numberOfStrokes, String symbolName, String symbolRomaji, XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "template");
        ArrayList<Tuple[]> tempList = new ArrayList<>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("stroke")) {
                tempList.add(readStroke(parser));
            } else {
                skip(parser);
            }
        }
        return new Symbol(numberOfStrokes, symbolName, symbolRomaji, tempList.toArray(new Tuple[0][]));
    }

    private Tuple[] readStroke(XmlPullParser parser) throws IOException, XmlPullParserException {
        ArrayList<Tuple> stroke = new ArrayList<>();
        parser.require(XmlPullParser.START_TAG, ns, "stroke");
        parser.nextTag();
        //while (parser.nextTag() != XmlPullParser.END_TAG) {
        while (!parser.getName().equals("stroke")) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("point")) {
                String x = parser.getAttributeValue(null, "x");
                String y = parser.getAttributeValue(null, "y");
                stroke.add(new Tuple(Integer.parseInt(x), -Integer.parseInt(y))); //y-axis points to the bottom in screen coordinates.
            } else {
                skip(parser);
            }
            if (parser.isEmptyElementTag())
            {
                parser.nextTag();
                parser.nextTag();
            }
        }
        parser.require(XmlPullParser.END_TAG, ns, "stroke");
        return stroke.toArray(new Tuple[0]);
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}

