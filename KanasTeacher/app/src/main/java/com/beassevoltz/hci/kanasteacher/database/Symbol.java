package com.beassevoltz.hci.kanasteacher.database;

import android.graphics.Path;
import android.graphics.PathMeasure;

import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.*;

public class Symbol 
{
	private int numberOfStrokes;
	private String name;
	private String romaji;
	private Tuple[][] strokes;
	private Path[] paths;
	private PathMeasure[] pathMeasures;
	private float length;
	
	public Symbol(int numberOfStrokes, String name, String romaji, Tuple[][] strokes)
	{
		this.numberOfStrokes = numberOfStrokes;
		this.name = name;
		this.romaji = romaji;
		this.strokes = strokes;
	}

	public int getNumberOfStrokes() { return this.numberOfStrokes; }

	public String getName()
	{
		return this.name;
	}

	public String getRomaji()
	{
		return this.romaji;
	}
	
	public Tuple[][] getStrokes()
	{
		return this.strokes;
	}

	public Path[] getPaths() { return this.paths; }

	public PathMeasure[] getPathMeasures() { return this.pathMeasures; }

	public float getLength() { return this.length; }

    /**
     * Initializes the paths, pathMeasures and the length of the kana.
     */
	public void toPath()
	{
		Tuple[][] strokes = rescale(this.strokes);
		paths = new Path[numberOfStrokes];
		pathMeasures = new PathMeasure[numberOfStrokes];

		for (int i = 0; i < numberOfStrokes; i++)
		{
			paths[i] = new Path();
			paths[i].moveTo((float) strokes[i][0].x, (float) strokes[i][0].y);
			for (int j = 0; j < strokes[i].length - 1; j++)
			{
				paths[i].lineTo((float) strokes[i][j+1].x, (float) strokes[i][j+1].y);
			}
			pathMeasures[i] = new PathMeasure(paths[i], false);
		}

        for (PathMeasure pm : pathMeasures)
        {
            length += pm.getLength();
        }
	}

	/**
	 * Rescales the strokes to fit the phone screen
	 * @param strokes is the array containing the strokes (each an array of Tuple).
     * @return The scaled strokes.
	 */
	private Tuple[][] rescale(Tuple[][] strokes)
	{
	    Tuple[][] scaledStrokes = strokes;

	    //finding the bounding box
		double minX = Double.MAX_VALUE;
		double maxX = - Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;
		double maxY = - Double.MAX_VALUE;
		for (Tuple[] stroke : strokes)
		{
			for (Tuple t : stroke)
			{
				if (minX > t.x) {minX = t.x;}
				if (maxX < t.x) {maxX = t.x;}
				if (minY > t.y) {minY = t.y;}
				if (maxY < t.y) {maxY = t.y;}
			}
		}

		int width = 900;
		int height = 900;
		double ratio = Math.min((width-20)/(maxX-minX), (height-20)/(maxY-minY));
		double yOffset = 120 - minY;
		double xOffset = 120 - minX;

		for (int i = 0; i < strokes.length; i++)
		{
			for (int j = 0; j < strokes[i].length; j++)
			{
				scaledStrokes[i][j].set(new Tuple(ratio * (strokes[i][j].x + xOffset), ratio * (strokes[i][j].y + yOffset)));
			}
		}
		return scaledStrokes;
	}

}
