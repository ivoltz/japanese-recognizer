package com.beassevoltz.hci.kanasteacher.nDollarRecognizer;

import java.io.Externalizable; 
import java.io.IOException; 
import java.io.ObjectInput; 
import java.io.ObjectOutput;
 
public class Unistroke implements Externalizable { 
  
 private Tuple[] points; 
 private Tuple startUnitVector; 
 private double[] vector; 
  
 
 public Unistroke() { } 
 
 public Unistroke(boolean useBoundedRotationInvariance, Tuple[] points) { 
  double radians = Util.indicativeAngle(points); 
  points = Util.resample(points, Util.NUM_POINTS); 
  points = Util.rotateBy(points, -radians); 
  points = Util.scaleDimTo(points, NDollarRecognizer.mSquareSize, Util.ONE_D_THRESHOLD); 
  if (useBoundedRotationInvariance) { 
   points = Util.rotateBy(points, radians); // restore 
  } 
  this.points = Util.translateTo(points, Util.ORIGIN); 
   
  this.startUnitVector = Util.calcStartUnitVector(this.points, Util.START_ANGLE_INDEX); 
  this.vector = Util.vectorize(this.points, useBoundedRotationInvariance); // for Protractor
 } 
  
 @Override 
 public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException { 
  this.points = new Tuple[input.readInt()]; 
  for (int i = 0; i < this.points.length; ++i) { 
   this.points[i] = new Tuple(input.readDouble(), input.readDouble()); 
  } 
  this.startUnitVector = new Tuple(input.readDouble(), input.readDouble()); 
  this.vector = new double[input.readInt()]; 
  for (int i = 0; i < this.vector.length; ++i) { 
   this.vector[i] = input.readDouble(); 
  } 
 } 
  
 @Override 
 public void writeExternal(ObjectOutput output) throws IOException { 
  output.writeInt(this.points.length); 
  for (Tuple p : this.points) { 
   output.writeDouble(p.x); 
   output.writeDouble(p.y); 
  } 
  output.writeDouble(this.startUnitVector.x); 
  output.writeDouble(this.startUnitVector.y); 
  output.writeInt(this.vector.length); 
  for (double d : this.vector) { 
   output.writeDouble(d); 
  } 
 } 
 
 public Tuple[] getPoints() { return this.points; } 
 
 public void setPoints(Tuple[] points) { this.points = points; } 
 
 public Tuple getStartUnitVector() { return this.startUnitVector; } 
 
 public void setStartUnitVector(Tuple startUnitVector) { this.startUnitVector = startUnitVector; } 
 
 public double[] getVector() { return this.vector; } 
 
 public void setVector(double[] vector) { this.vector = vector; } 
  
}