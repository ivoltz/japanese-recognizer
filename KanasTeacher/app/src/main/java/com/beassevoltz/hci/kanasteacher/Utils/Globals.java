package com.beassevoltz.hci.kanasteacher.Utils;

import android.util.Log;

import com.beassevoltz.hci.kanasteacher.database.Symbol;
import com.beassevoltz.hci.kanasteacher.database.XmlParser;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Multistroke;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.NDollarRecognizer;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Tuple;

import java.io.InputStream;
import java.util.ArrayList;

public class Globals {

    private static Globals instance;

    private NDollarRecognizer recognizer;
    private String[] names = new String[71];
    private ArrayList<Symbol> templates;
    private String[] romaji = new String[71];

    private Globals() {}

    public static synchronized Globals getInstance()
    {
        if (instance == null)
        {
            instance = new Globals();
        }
        return instance;
    }

    public NDollarRecognizer getRecognizer()
    {
        return recognizer;
    }

    public String[] getNames() { return names; }

    public String[] getRomaji() { return romaji; }

    public ArrayList<Symbol> getTemplates() { return templates; }

    /**
     * Processes all symbols from the kanas.xml and passes them through the recognizer.
     * @param is is the kanas.xml file we have in the resources,
     *           where all the strokes are defined.
     */
    public void setupRecognizer(InputStream is)
    {
        ArrayList<Multistroke> baseSymbols = new ArrayList<>();

        int[] indices = new int[71];

        XmlParser xmlParser = new XmlParser();
        try
        {
            templates = xmlParser.parse(is);
            int index = 0;
            for (Symbol s : templates)
            {
                names[index] = s.getName();
                romaji[index] = s.getRomaji();
                indices[index] = index;
                Log.e("test", names[index] + "; index = " + String.valueOf(index));

                if(index == 30 || index == 47 || index == 49 || index > 72)
                {
                    baseSymbols.add(new Multistroke(index, true, new Tuple[0][]));
                } else
                {
                    baseSymbols.add(new Multistroke(index, true, s.getStrokes()));
                }

                index++;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        recognizer = new NDollarRecognizer(600, true);

        recognizer.setMultistrokes(baseSymbols);
        recognizer.setUseIndices(indices);
        templatesToPath();
    }

    /**
     * Converts every to kana to a list of paths.
     */
    private void templatesToPath()
    {
        for (Symbol kana : templates)
        {
            kana.toPath();
        }
    }

    /**
     * Converts the score returned by the recognizer to a score between 0 and 100.
     * It is a concatenation of four affine functions.
     * @param score is the score returned by the recognizer, that is
     *              the inverse of the sum of the distances between
     *              the points of the model and the points drawn by the user.
     * @param kana is the kana on which the score is tested.
     * @return A integer score between 0 and 100
     */
    public static int trueScore(double score, Symbol kana)
    {
        int newScore = 0;
        int upperBound = 17000;
        int mediumBound = 6000;
        int lowerBound = 200;
        float length = kana.getLength();
        double normalizedScore = score * length;
        if (normalizedScore < lowerBound) newScore = 0;
        if (normalizedScore > lowerBound && normalizedScore < mediumBound)
        {
            newScore = (int) ((normalizedScore-lowerBound)/(mediumBound-lowerBound) * 50);
        }
        if (normalizedScore > mediumBound && normalizedScore < upperBound)
        {
            newScore = (int) ((normalizedScore-mediumBound)/(upperBound-mediumBound) * 50) + 50;
        }
        if (normalizedScore > upperBound) newScore = 100;
        return newScore;
    }
}
