package com.beassevoltz.hci.kanasteacher.nDollarRecognizer;

import android.util.Log;

import java.io.Externalizable;
import java.io.IOException; 
import java.io.ObjectInput; 
import java.io.ObjectOutput; 
import java.util.ArrayList; 

public class Multistroke implements Externalizable { 
  
 private int type; 
 private int numStrokes; 
 private Unistroke[] unistrokes; 
 private Tuple[][] origStrokes; 
  
 
 public Multistroke() { } 
 
 public Multistroke(int type, boolean useBoundedRotationInvariance, Tuple[][] strokes) { 
  this.type = type; 
  this.numStrokes = strokes.length; // number of individual strokes 
  this.origStrokes = strokes; 
   
  int[] order = new int[strokes.length]; 
  for (int i = 0; i < strokes.length; ++i) { 
   order[i] = i; // initialize 
  }

  ArrayList<Integer[]> ordersList = new ArrayList<>(); 
  Util.heapPermute(strokes.length, order, ordersList);

  int[][] orders = new int[ordersList.size()][]; 
  int count = 0; 
  for (Integer[] array : ordersList) { 
   int[] inner = new int[array.length]; 
   for (int i = 0; i < array.length; ++i) { 
    inner[i] = array[i]; 
   } 
   orders[count++] = inner; 
  }
 
  Tuple[][] unistrokes = Util.makeUnistrokes(strokes, orders);

  this.unistrokes = new Unistroke[unistrokes.length]; // unistrokes for this multistroke

  for (int j = 0; j < unistrokes.length; ++j) {
   this.unistrokes[j] = new Unistroke(useBoundedRotationInvariance, unistrokes[j]); 
  } 
 } 
  
 @Override 
 public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException { 
  this.type = input.readInt(); 
  this.numStrokes = input.readInt(); 
  this.unistrokes = new Unistroke[input.readInt()]; 
  for (int i = 0; i < this.unistrokes.length; ++i) { 
   this.unistrokes[i] = (Unistroke) input.readObject(); 
  } 
  this.origStrokes = new Tuple[input.readInt()][]; 
  for (int i = 0; i < this.origStrokes.length; ++i) { 
   this.origStrokes[i] = new Tuple[input.readInt()]; 
   for (int j = 0; j < this.origStrokes[i].length; ++j) { 
    this.origStrokes[i][j] = new Tuple(input.readDouble(), input.readDouble()); 
   } 
  } 
 } 
  
 @Override 
 public void writeExternal(ObjectOutput output) throws IOException { 
  output.writeInt(this.type); 
  output.writeInt(this.numStrokes); 
  output.writeInt(this.unistrokes.length); 
  for (Unistroke u : this.unistrokes) { 
   output.writeObject(u); 
  } 
  output.writeInt(this.origStrokes.length); 
  for (Tuple[] points : this.origStrokes) { 
   output.writeInt(points.length); 
   for (Tuple p : points) { 
    output.writeDouble(p.x); 
    output.writeDouble(p.y); 
   } 
  } 
 } 
 
 public int getType() { return this.type; } 
 
 public void setType(int type) { this.type = type; } 
 
 public int getNumStrokes() { return this.numStrokes; } 
 
 public void setNumStrokes(int numStrokes) { this.numStrokes = numStrokes; } 
 
 public Unistroke[] getUnistrokes() { return this.unistrokes; } 
 
 public void setUnistrokes(Unistroke[] unistrokes) { this.unistrokes = unistrokes; } 
 
 public Tuple[][] getOrigStrokes() { return this.origStrokes; } 
 
 public void setOrigStrokes(Tuple[][] origStrokes) { this.origStrokes = origStrokes; } 
  
}