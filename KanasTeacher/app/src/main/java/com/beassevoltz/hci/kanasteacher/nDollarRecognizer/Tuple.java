package com.beassevoltz.hci.kanasteacher.nDollarRecognizer;

public class Tuple 
{
	public double x;
	public double y;
	
	public Tuple() 
	{
		x=0.0;
		y=0.0;
	}

	public Tuple(double x0,double y0) 
	{
		x=x0;
		y=y0;
	}
	
	public Tuple(Tuple a) 
	{
		x=a.x;
		y=a.y;
	}
	
	
	public Tuple add(Tuple a) 
	{
		x += a.x;
		y += a.y;
		return this;
	}
	
	public Tuple sub(Tuple a) 
	{
		x -= a.x;
		y -= a.y;
		return this;
   	}
  
	public double norm() 
	{
		return Math.sqrt(x*x + y*y);
	}

	public void normalize() 
	{
		double n = norm();
		x *= 1.0/n;
		y *= 1.0/n;
	}
  
	public Tuple add(Tuple a, Tuple b) 
	{
		x = a.x + b.x;
		y = a.y + b.y;
		return this;
	}
    
	public void set(Tuple a) 
	{
		x = a.x;
		y = a.y;
	}
    
    public void copy(Tuple a) 
    {
    	a.x = x;
    	a.y = y;
    }
    
}