package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beassevoltz.hci.kanasteacher.R;
import com.beassevoltz.hci.kanasteacher.Utils.Globals;
import com.beassevoltz.hci.kanasteacher.drawing.DrawKanaView;
import com.beassevoltz.hci.kanasteacher.drawing.ShowKanaView;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.NDollarRecognizer;

public class LearnKanaActivity extends AppCompatActivity {

    private NDollarRecognizer recognizer;
    private int index;
    private boolean practicing;
    private String[] romaji;

    private LinearLayout container;
    private Button previousButton;
    private Button nextButton;
    private TextView kanaName;
    private Button scoreButton;
    private Button modeButton;
    private ShowKanaView showKana;
    private DrawKanaView drawKana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_kana);
        Intent intent = getIntent();
        index = intent.getIntExtra("index", 0);
        recognizer = Globals.getInstance().getRecognizer();
        practicing = false;

        setupUI();
    }

    private void setupUI()
    {
        container =  findViewById(R.id.learn_draw_container);
        romaji = Globals.getInstance().getRomaji();

        previousButton = findViewById(R.id.previousButton);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previous(v);
            }
        });
        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next(v);
            }
        });
        modeButton = findViewById(R.id.modeButton);
        modeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchMode(v);
            }
        });
        if (index == 30 || index == 47 || index == 49)
        {
            //the now infamous particular cases...
            modeButton.setEnabled(false);
        }
        scoreButton = findViewById(R.id.scoreButton);

        kanaName = findViewById(R.id.learn_kana_name);
        kanaName.setText(romaji[index]);

        showKana = new ShowKanaView(getApplicationContext(), index);
        drawKana = new DrawKanaView(getApplicationContext(), recognizer, index);
        drawKana.setRecognizeButton(scoreButton, kanaName);
        container.addView(showKana);
    }

    /**
     * Called when the "previous" button is pressed
     */
    private void previous (View v)
    {
        //This method could be a lot shorter if we didn't have to treat the 3 cases
        //where the recognizer won't load the kanas (da, bu, bo).
        //Basically we take the previous element in the Z/71Z group.
        if(practicing)
        {
            if (index == 0)
            {
                index = 70;
            } else if (index == 31 || index == 48 || index == 50)
            {
                index = index - 2;
            } else
            {
                index = index - 1;
            }
            drawKana.updateView(index);
        } else
        {
            if (index == 0)
            {
                index = 70;
            } else
            {
                modeButton.setEnabled(true);
                index = index - 1;
                if (index == 30 || index == 47 || index == 49)
                {
                    modeButton.setEnabled(false);
                }
            }
            showKana.updateView(index);
        }
        kanaName.setText(romaji[index]);

    }

    /**
     * Called when the "next" button is pressed
     */
    private void next(View v)
    {
        //This method could be a lot shorter if we didn't have to treat the 3 cases
        //where the recognizer won't load the kanas (da, bu, bo).
        //Basically we take the next element in the Z/71Z group.
        if(practicing)
        {
            if (index == 70)
            {
                index = 0;
            } else if (index == 29 || index == 46 || index == 48)
            {
                index = index + 2;
            } else
            {
                index = index + 1;
            }
            drawKana.updateView(index);
        } else
        {
            if (index == 70)
            {
                index = 0;
            } else
            {
                modeButton.setEnabled(true);
                index = index + 1;
                if (index == 30 || index == 47 || index == 49)
                {
                    modeButton.setEnabled(false);
                }

            }
            showKana.updateView(index);
        }
        kanaName.setText(romaji[index]);
    }

    /**
     * Called when the "Practice" or "Learn" button is pressed to switch between these two modes.
     */
    private void switchMode(View v)
    {
        if (practicing)
        {
            //we put the view where the drawing of the kana is shown.
            //The score button becomes invisible.
            modeButton.setText(R.string.practice);
            scoreButton.setVisibility(Button.INVISIBLE);
            container.removeView(drawKana);
            container.addView(showKana);

            kanaName.setText(romaji[index]);
            showKana.updateView(index);
        } else
        {
            //we put the view where the drawing of the kana is shown.
            //The score button becomes invisible.
            modeButton.setText(R.string.learn);
            scoreButton.setVisibility(Button.VISIBLE);
            container.removeView(showKana);
            container.addView(drawKana);

            kanaName.setText(romaji[index]);
            drawKana.updateView(index);
        }

        practicing = !practicing;
    }



}
