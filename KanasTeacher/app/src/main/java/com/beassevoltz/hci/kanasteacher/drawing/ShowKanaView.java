package com.beassevoltz.hci.kanasteacher.drawing;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.beassevoltz.hci.kanasteacher.Utils.Globals;
import com.beassevoltz.hci.kanasteacher.database.Symbol;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Tuple;

import java.util.ArrayList;

/**
 * Created by fbeasse on 18/10/18.
 */

public class ShowKanaView extends View
{
    private int index;
    private Symbol kana;
    private int numberOfStrokes;

    private Path[] paths;
    private PathMeasure[] pathMeasures;
    private Path currentPath = new Path();

    private Paint drawPaint, canvasPaint;
    private int paintColor = 0xFF660000;
    private Bitmap canvasBitmap;

    private AnimatorSet animatorSet = new AnimatorSet();

    public ShowKanaView(Context context, int index)
    {
        super(context);
        setupDrawing();
        updateView(index);
    }

    /**
     * Defines the painting characteristics.
     */
    private void setupDrawing()
    {
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(60);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);

        //painting the kana in transparency so the user can see it complete before the animation has ended.
        int transparentColor = 0x33660000;
        drawPaint.setColor(transparentColor);
        for (Path path : paths)
        {
            canvas.drawPath(path, drawPaint);
        }
        //resetting the painting color
        drawPaint.setColor(paintColor);

        //code fragment that allows animation of the stroke painting
        long duration = animatorSet.getCurrentPlayTime();
        int currentAnimation = (int) duration/2000;
        if (!animatorSet.isRunning())
        {
            currentAnimation = numberOfStrokes;
        }
        for (int i = 0; i < currentAnimation; i++)
        {
            canvas.drawPath(paths[i], drawPaint);
        }
        canvas.drawPath(currentPath, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                animatorSet.end();
                animatorSet.start();
                break;
            default:
                return false;
        }
        return true;
    }

    /**
     * called whenever the index change
     * @param index is the index of the kana in the list stored in Globals.
     */
    public void updateView(int index)
    {
        animatorSet.end();
        this.index = index;
        kana = Globals.getInstance().getTemplates().get(index);
        numberOfStrokes = kana.getNumberOfStrokes();
        paths = kana.getPaths();
        pathMeasures = kana.getPathMeasures();
        setupAnimators();
        animatorSet.start();
    }

    /**
     * Creates an ValueAnimator. There is one per stroke.
     * @param i the index of the stroke in the drawing of the kana (there is an order).
     */
    private ValueAnimator createAnimator(final int i) {
        ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setDuration(2000);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float val = valueAnimator.getAnimatedFraction();
                currentPath.reset();
                pathMeasures[i].getSegment(0, pathMeasures[i].getLength() * val, currentPath, true);

                invalidate();
            }
        });
        return animator;
    }

    /**
     * Puts all animators together in an animatorSet to play the entire kana.
     */
    private void setupAnimators()
    {
        animatorSet = new AnimatorSet();
        ArrayList<Animator> vaList = new ArrayList<Animator>();
        for (int i = 0; i < numberOfStrokes; i++)
        {
            vaList.add(createAnimator(i));
        }
        animatorSet.playSequentially(vaList);
    }
}
