package com.beassevoltz.hci.kanasteacher.drawing;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.beassevoltz.hci.kanasteacher.R;
import com.beassevoltz.hci.kanasteacher.Utils.Globals;
import com.beassevoltz.hci.kanasteacher.activities.KanaChoiceActivity;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.*;

import java.util.ArrayList;

public class DrawView extends View {

    private ArrayList<Tuple[]> strokes;
    private ArrayList<Tuple> currStroke;

    private NDollarRecognizer recognizer;

    private Path drawPath;
    private Paint drawPaint, canvasPaint;
    private int paintColor = 0xFF660000;
    private Canvas drawCanvas;
    private Bitmap canvasBitmap;

    private String[] names;

    public DrawView(Context context, NDollarRecognizer recognizer)
    {
        super(context);
        this.recognizer = recognizer;
        this.names = Globals.getInstance().getNames();
        setupDrawing();
    }

    public DrawView(Context context)
    {
        super(context);
        setupDrawing();
    }

    public DrawView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setupDrawing();
    }

    public int getStrokesNumber()
    {
        return strokes.size();
    }

    public ArrayList<Tuple[]> getStrokes()
    {
        return strokes;
    }

    /**
     * Setup the user drawing
     */
    private void setupDrawing()
    {
        drawPath = new Path();
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);

        strokes = new ArrayList<>();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        /// Draw the stroke as the user moves on the screen

        float touchX = event.getX();
        float touchY = event.getY();

        // Create a new stroke, add point to the stroke or end the stroke depending on the user interaction event
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                currStroke = new ArrayList<>();
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                currStroke.add(new Tuple(touchX, touchY));
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                strokes.add(currStroke.toArray(new Tuple[0]));
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    /**
     * Clear the drawing area
     */
    public void clearView()
    {
        strokes.clear();
        drawPath.reset();
        invalidate();
        drawCanvas.drawColor(Color.WHITE);
    }
}
