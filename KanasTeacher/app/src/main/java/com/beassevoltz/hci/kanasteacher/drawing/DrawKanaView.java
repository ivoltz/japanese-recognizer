package com.beassevoltz.hci.kanasteacher.drawing;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.beassevoltz.hci.kanasteacher.Utils.Globals;
import com.beassevoltz.hci.kanasteacher.database.Symbol;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.NDollarRecognizer;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Tuple;

import java.util.ArrayList;

/**
 * Created by fbeasse on 19/10/18.
 */

public class DrawKanaView extends View
{
    private int index;
    private Symbol kana;

    private Path[] paths;

    private ArrayList<Tuple[]> strokes;
    private ArrayList<Tuple> currStroke;

    private NDollarRecognizer recognizer;

    private Path drawPath;
    private Paint drawPaint, canvasPaint;
    private int paintColor = 0xFF660000;
    private Canvas drawCanvas = null;
    private Bitmap canvasBitmap;

    private String[] romaji;

    public DrawKanaView(Context context, NDollarRecognizer recognizer, int index)
    {
        super(context);
        this.recognizer = recognizer;
        this.romaji = Globals.getInstance().getRomaji();
        setupDrawing();
        updateView(index);
    }

    /**
     * Defines the painting characteristics.
     */
    private void setupDrawing()
    {
        drawPath = new Path();
        drawPaint = new Paint();

        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(60);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);

        strokes = new ArrayList<>();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);

        //painting the kana in transparency so the user can practice.
        int transparentColor = 0x33660000;
        drawPaint.setColor(transparentColor);
        for (Path path : paths)
        {
            canvas.drawPath(path, drawPaint);
        }

        //resetting the painting color
        drawPaint.setColor(paintColor);

        //draw what the user is currently drawing;
        canvas.drawPath(drawPath, drawPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float touchX = event.getX();
        float touchY = event.getY();

        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                currStroke = new ArrayList<>();
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                currStroke.add(new Tuple(touchX, touchY));
                break;
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                strokes.add(currStroke.toArray(new Tuple[0]));
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    /**
     * called whenever the index change
     * @param index is the index of the kana in the list stored in Globals.
     */
    public void updateView(int index)
    {
        this.index = index;
        kana = Globals.getInstance().getTemplates().get(index);
        paths = kana.getPaths();

        recognizer.setUseIndex(index);

        strokes.clear();
        drawPath.reset();
        if (drawCanvas != null)
            drawCanvas.drawColor(Color.WHITE);

        invalidate();
    }

    /**
     * Assigns a textView to a button to print the result given by the recognizer.
     * @param button will call the recognizer when pressed.
     * @param scoreText will display the result.
     */
    public void setRecognizeButton(Button button, final TextView scoreText)
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strokes.size() > 0) {
                    NDollarRecognizer.Result result = recognizer.recognize(strokes.toArray(new Tuple[0][]), true, true);
                    if (result != null) {
                        scoreText.setText(romaji[result.pose] + " - " + String.valueOf(Globals.trueScore(result.score, kana)));
                    } else {
                        scoreText.setText("Didn't work");
                    }
                    strokes.clear();
                    drawPath.reset();
                    drawCanvas.drawColor(Color.WHITE);
                    invalidate();
                }
            }
        });
    }
}
