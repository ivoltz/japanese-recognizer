package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.beassevoltz.hci.kanasteacher.R;
import com.beassevoltz.hci.kanasteacher.Utils.Globals;

public class KanaChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kana_choice);
        Intent intent = getIntent();

        setup();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setup()
    {
        //gets the data
        Globals globals = Globals.getInstance();
        String[] hiraganas = globals.getNames();
        String[] romaji = globals.getRomaji();

        //gets the container defined in the XML file
        final TableRow.LayoutParams OM = new TableRow.LayoutParams(
                0, TableRow.LayoutParams.MATCH_PARENT, 1);

        TableLayout container = findViewById(R.id.container_layout);
        TableRow tableRow = new TableRow(this);
        TableRow textTableRow = new TableRow(this);

        //iterates on the 71 hiraganas
        for(int i = 0; i < 71; i++)
        {
            //creates a tablerow with 5 columns
            if (i%5 == 0)
            {
                tableRow = new TableRow(this);
                textTableRow = new TableRow(this);
            }
            //creates button which will launch KanaChoiceActivity
            Button button = new Button(this);
            button.setId(i);
            button.setText(hiraganas[i]);
            button.setTextSize(30);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(KanaChoiceActivity.this, LearnKanaActivity.class);
                    intent.putExtra("index", v.getId());
                    startActivity(intent);
                }
            });
            tableRow.addView(button, OM);

            //creates romaji textview so the user knows the name of the hiragana he is choosing.
            TextView text = new TextView(this);
            text.setText(romaji[i]);
            text.setGravity(Gravity.CENTER);
            textTableRow.addView(text, OM);


            //adds tablerow to the container
            if (i%5 == 4 || i == 70)
            {
                container.addView(tableRow);
                container.addView(textTableRow);
            }
        }
    }
}
