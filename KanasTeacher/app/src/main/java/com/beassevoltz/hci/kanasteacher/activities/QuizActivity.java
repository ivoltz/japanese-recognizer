package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beassevoltz.hci.kanasteacher.R;
import com.beassevoltz.hci.kanasteacher.Utils.Globals;
import com.beassevoltz.hci.kanasteacher.database.Symbol;
import com.beassevoltz.hci.kanasteacher.drawing.DrawView;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.NDollarRecognizer;
import com.beassevoltz.hci.kanasteacher.nDollarRecognizer.Tuple;

import java.util.ArrayList;
import java.util.Random;

public class QuizActivity extends AppCompatActivity {

    static String QUIZ_ACTIVITY = "Quiz Activity";

    private NDollarRecognizer recognizer;
    private String[] names;
    private String[] romaji;
    private ArrayList<Symbol> symbols;
    private ArrayList<Pair<Boolean, Integer>> quizIndices; // false: Draw kana from romaji, true: Guess romaji from kana

    private int score = 0;

    private int currSymbol = -1;

    private TextView questionText;
    private TextView questionText2;

    private DrawView drawView;
    private EditText answerView;

    private Button okButton;
    private Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        // Retrieve the quiz type
        Intent intent = getIntent();
        int quizType = intent.getIntExtra("type", 0);

        // Get the needed symbols properties
        Globals globals = Globals.getInstance();
        recognizer = globals.getRecognizer();
        names = globals.getNames();
        romaji = globals.getRomaji();
        symbols = globals.getTemplates();

        // Setup question text
        questionText = findViewById(R.id.questionText);
        questionText2 = findViewById(R.id.questionText2);

        // Setup answer zones
        drawView = new DrawView(getApplicationContext(), recognizer);
        answerView = new EditText(getApplicationContext());

        answerView.setHint(R.string.answer_hint);
        answerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        answerView.setPadding(20, 20, 20, 20);
        answerView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        answerView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    validateAnswer(quizIndices.get(currSymbol).first);
                return true;
            }
        });


        // Setup validation and reset buttons
        okButton = new Button(getApplicationContext());
        okButton.setText(R.string.ok);
        resetButton = new Button(getApplicationContext());
        resetButton.setText(R.string.reset);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAnswer(quizIndices.get(currSymbol).first);
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.clearView();
            }
        });

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        param.gravity = Gravity.CENTER;

        okButton.setLayoutParams(param);
        resetButton.setLayoutParams(param);

        createQuiz(quizType);

        nextScreen();
    }

    /**
     * Sets up the quiz by generating a random list of symbols, the type of the question depends on the param
     * @param type The type of question (symbol or pronunciation)
     */
    private void createQuiz(int type)
    {
        quizIndices = new ArrayList<>();
        int indNum = names.length;
        Random randGen = new Random();

        for (int i = 0; i < 10; i++)
        {
            int index = randGen.nextInt(indNum);

            // Remove unusable kanas
            if(index == 30 || index == 47 || index == 49)
                i--;
            else
            {
                if (type == 2)
                    quizIndices.add(new Pair<>(randGen.nextBoolean(), index));
                else
                    quizIndices.add(new Pair<>(type == 1, index));
            }
        }
    }

    /**
     * Clears the question and sets up the next one according to the next question
     */
    private void nextScreen() {
        currSymbol++; // Keep track of which question is currently played

        if (currSymbol >= 10)
        {
            Toast.makeText(getApplicationContext(), "Final score: " + score, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Clear test screen
        ConstraintLayout quizLayout = findViewById(R.id.quizLayout);
        LinearLayout validateLayout = findViewById(R.id.validateLayout);

        quizLayout.removeAllViews();
        validateLayout.removeAllViews();

        // Setup screen depending on question type
        if (!quizIndices.get(currSymbol).first)
        {
            questionText.setText(R.string.draw_kana);
            questionText2.setText(romaji[quizIndices.get(currSymbol).second]);
            quizLayout.addView(drawView);
            validateLayout.addView(resetButton);
            validateLayout.addView(okButton);
        }
        else
        {
            questionText.setText(R.string.guess_romaji);
            questionText2.setText(names[quizIndices.get(currSymbol).second]);
            quizLayout.addView(answerView);
            validateLayout.addView(okButton);

            // Force the keyboard to appear
            answerView.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(answerView, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Check if answer is valid and adds the score given
     * @param questionType The type of the question
     */
    private void validateAnswer(boolean questionType)
    {
        if (!questionType)
        {
            if (drawView.getStrokesNumber() == 0)
            {
                // Asks the user to draw something if nothing is drawn
                Toast.makeText(getApplicationContext(), "Please draw a symbol", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                // Use the recognizer to give a score and add it to the total
                int[] inds = { quizIndices.get(currSymbol).second };
                recognizer.setUseIndices(inds); // Asks the reocgnizer to only recognize the current symbol
                NDollarRecognizer.Result result = recognizer.recognize(drawView.getStrokes().toArray(new Tuple[0][]), true, true);
                int questionScore = getScore(result);
                score += questionScore;
                Toast.makeText(getApplicationContext(), "Your score is: " + questionScore, Toast.LENGTH_SHORT).show();
                drawView.clearView();
            }
        }
        else
        {
            // Get the text written, check if it is right (or empty) and add score
            String answer = answerView.getText().toString();
            if (answer.isEmpty())
            {
                Toast.makeText(getApplicationContext(), "Please type an answer", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                if (answerView.getText().toString().equals(romaji[quizIndices.get(currSymbol).second]))
                {
                    score += 100;
                    Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), "Wrong! The answer was: " + romaji[quizIndices.get(currSymbol).second], Toast.LENGTH_SHORT).show();
                answerView.getText().clear();
            }
        }

        nextScreen(); // Setup next question
    }

    /**
     * Returns the adapted score from the result given by the recognizer
     * @param result Result given by the recognizer
     * @return The actual adapted score
     */
    private int getScore(NDollarRecognizer.Result result)
    {
        if (result == null)
            return 0;
        else
        {
            Log.i(QUIZ_ACTIVITY, "Score is not null: " + result.score);
            return Globals.trueScore(result.score, symbols.get(currSymbol));
        }
    }
}
