package com.beassevoltz.hci.kanasteacher.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.beassevoltz.hci.kanasteacher.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        // Setup the buttons
        final ImageButton symbolsButton = findViewById(R.id.symbolsTest);
        final ImageButton pronunciationButton = findViewById(R.id.pronunciationTest);
        final Button bothButton = findViewById(R.id.bothTest);

        // Symbols test button
        symbolsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, QuizActivity.class);
                intent.putExtra("type", 0);
                startActivity(intent);
            }
        });

        // Pronunciation test button
        pronunciationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, QuizActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });

        // Symbols and pronunciation test button
        bothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, QuizActivity.class);
                intent.putExtra("type", 2);
                startActivity(intent);
            }
        });
    }
}
